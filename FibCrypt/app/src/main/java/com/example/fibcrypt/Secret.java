package com.example.fibcrypt;

import java.util.ArrayList;
import java.util.List;

public class Secret {
    List<Integer> fibo;
    int CHAR_MAX=127;
    public Secret(){
        double phi1 = (1 + Math.sqrt(5)) / 2, phi2 = (1 - Math.sqrt(5)) / 2;
        fibo = new ArrayList<>();
        for (int i = 1; i < 13; i++)
            fibo.add((int)((Math.pow(phi1, 3 * i + 2) - Math.pow(phi2, 3 * i + 2)) / (2 * Math.sqrt(5)) - 1 / 2));
    }

    private String reverseString(String x) {
        String r="";
        for (int i = 0; i < x.length(); i++) r+= x.charAt(i);
        return r;
    }

    private char shift(char x, int n) {
        return (char) ((x - 32 + n % CHAR_MAX + CHAR_MAX - 32) % (CHAR_MAX - 32) + 32);
    }

    private String cesar(String x, Boolean encripting) {
        String r = "";
        int enc = encripting ? 1 : -1;
        int len = x.length() % fibo.size();
        for (int i = 0; i < x.length(); i++)
            r += shift(x.charAt(i), enc*this.fibo.get(len));
        return r;
    }

    public String encrypt(String x) {
        x = cesar(x, true);
        return x.length() == 1 ? x :
                encrypt(reverseString(x.substring(x.length() / 2)))+
                        encrypt(reverseString(x.substring(0, x.length() / 2)));
    }

    public String decrypt(String x) {
        return cesar(
                x.length() == 1 ? x :
                        decrypt(reverseString(x.substring(x.length() / 2))) +
                                decrypt(reverseString(x.substring(0, x.length() / 2))),
                false
        );
    }

}
