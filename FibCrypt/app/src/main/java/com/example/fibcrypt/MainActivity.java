package com.example.fibcrypt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<View> vs;
    Secret s;
    Boolean encrypt;
    void Click(){
        String msg=((EditText)(vs.get(0))).getText().toString();
        if(msg.length()%2==1) msg+=" ";
        ((EditText)vs.get(1)).setText(encrypt? s.encrypt(msg): s.decrypt(msg));
    }

    void Toggle(){
        encrypt=!encrypt;
        ((Button)(vs.get(2))).setText(encrypt? "Encrypt":"Decrypt");
        ((EditText)(vs.get(0))).setHint((encrypt?"Plain":"Secret")+" Text");
        ((EditText)(vs.get(1))).setHint((!encrypt?"Plain":"Secret")+" Text");
        ((EditText)(vs.get(0))).getText().clear();
        ((EditText)(vs.get(1))).getText().clear();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LinearLayout ll=findViewById(R.id.mainLayout);
        s=new Secret();
        encrypt=true;
        View tmp;
        vs=new ArrayList<>();
        for (int i=0;i<4;i++)
        {
            tmp=i<2?new EditText(ll.getContext()):new Button(ll.getContext());
            if(i<2)  ((EditText)tmp).setHint((i==0?"Plain":"Secret")+" Text");
            else if(i==2)
            {
                ((Button)tmp).setText("Encrypt");
                tmp.setOnClickListener(v-> Click());
            }
            else
            {
                ((Button)tmp).setText("Toggle");
                tmp.setOnClickListener(v-> Toggle());
            }
            tmp.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            vs.add(tmp);
            ll.addView(tmp);
        }
    }
}