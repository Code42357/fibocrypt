#include <vector>
#include <string>
#include <iostream>
#include <algorithm>    
#include <climits>

using namespace std;

vector<int> fibo;

void computeFibo() {
    double phi1 = (1 + sqrt(5)) / 2, phi2 = (1 - sqrt(5)) / 2;
    for (int i = 1; i < 13; i++)
        fibo.push_back((pow(phi1, 3 * i + 2) - pow(phi2, 3 * i + 2)) / (2 * sqrt(5)) - 1 / 2);
}

string reverseString(string x) {    
    reverse(x.begin(), x.end());    
    return x;
}

char shift(char x, int n) {
    return (x - 32 + n % CHAR_MAX + CHAR_MAX - 32) % (CHAR_MAX - 32) + 32;
}

string cesar(string x, bool encrypt=false) {
    string r = "";
    int enc = encrypt ? 1 : -1;
    int len = x.length() % fibo.size();    
    for (int i = 0; i < x.length(); i++)
        r += shift(x[i], enc*fibo[len]);
    return r;
}

string encrypt(string x) {    
    x = cesar(x, true);
    return x.length() == 1 ? x :    
        encrypt(reverseString(x.substr(x.length() / 2, x.length())))+
        encrypt(reverseString(x.substr(0, x.length() / 2)));
}

string decrypt(string x) {            
    return cesar(
        x.length() == 1 ? x :
        decrypt(reverseString(x.substr(x.length() / 2, x.length()))) +
        decrypt(reverseString(x.substr(0, x.length() / 2)))
    );
}


int main() {
    computeFibo();
    string mensaje = "abc.";    
    string secret = encrypt(mensaje);
    cout << mensaje << "\n";
    cout << secret << "\n";
    cout << decrypt(secret);
    return 0;
}